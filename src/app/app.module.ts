import { Usluga } from './Usluga.service';

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { HomeScrenComponent } from './home-scren/home-scren.component';
import { DayScreenComponent } from './day-screen/day-screen.component';
import { HttpModule } from '@angular/http';
import { DaysComponent } from './day-screen/days/days.component';


const appRouts: Routes = [
  {path: "", component: HomeScrenComponent},
  {path: "DayScreen", component: DayScreenComponent},
  

]


@NgModule({
  declarations: [
    AppComponent,
    HomeScrenComponent,
    DayScreenComponent,
    DaysComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRouts),
    HttpModule
  ],
  providers: [Usluga],
  bootstrap: [AppComponent]
})
export class AppModule { }
