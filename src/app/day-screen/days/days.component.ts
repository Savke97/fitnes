import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { $ } from 'protractor';






@Component({
  selector: 'app-days',
  templateUrl: './days.component.html',
  styleUrls: ['./days.component.css']
})
export class DaysComponent implements OnInit {

  nizDana: String[] = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday'];
  @Output() izabranDan: EventEmitter<String> = new EventEmitter();


  dan:String;

  constructor() { }

  ngOnInit() {
  }

  

  onDayClicked(Dani: String){
    this.dan = Dani;
    this.nizDana.forEach(element => {
      if(Dani === element){
        this.izabranDan.emit(Dani);
      }
    });
  }


}
