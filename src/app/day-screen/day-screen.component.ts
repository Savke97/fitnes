import { Usluga } from './../Usluga.service';
import { Http, Response } from '@angular/http';
import { Component, OnInit, Input } from '@angular/core';
import { DaysComponent } from './days/days.component';

@Component({
  selector: 'app-day-screen',
  templateUrl: './day-screen.component.html',
  styleUrls: ['./day-screen.component.css']
})
export class DayScreenComponent implements OnInit {


  nizDana: String[] = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday'];
  constructor(private http: Http, private service: Usluga,){}


  i: number;
  BrKoraka: number = 0;
  PotrosenoKalorija: Number;
  Dan: String = this.service.DanIzbran;
  DatumIzbranogDana: Number;
  Vreme: number;
  Sat: Number;
  Kilometari: number;
  Datum: any;
  odredjenDanizabran: number;
  Minuti:number;
  Koraci: number;
  Recenica: String;
  Iskaz: String;





  ngOnInit() {
    this.onIzabranDan(this.Dan);
  }

  

  onIzabranDan(event: String){
    this.Dan = event;

      this.http.get('https://api.myjson.com/bins/1gwnal?fbclid=IwAR1jM8e48gXZylO9n3KzN1Dyxbqosi7y1vRSd9p9XMnrvUKPspiS0y0_lyo/data.json')
    .subscribe((res: Response) => {
        const data = res.json();

       
          for( this.i =0; this.i<this.nizDana.length; this.i++ ){
            if(this.nizDana[this.i] === this.Dan){
              this.odredjenDanizabran = this.i + 1;
              this.DatumIzbranogDana = this.i +10;
            }
        }
          
          
                data.forEach((el, i) => {
                this.Datum = new Date(el.timestamp).getDay();
                if(this.Datum === this.odredjenDanizabran){
                  this.BrKoraka = this.BrKoraka + el.steps;
                  

                  /* Ispis poruke u zavisnosti od broja koraka */
                            if(this.BrKoraka <= 1000){
                              this.Recenica = 'Bad';
                              this.Iskaz = 'You have to do better!'
                          }
                            else if(this.BrKoraka <= 2000){
                              this.Recenica = 'Not Bad';
                              this.Iskaz = 'You got better!'
                          }
                          else if(this.BrKoraka <= 3000){
                            this.Recenica = 'Good';
                            this.Iskaz = 'Keep going!'
                        }
                        else if(this.BrKoraka <= 4000){
                          this.Recenica = 'Nice';
                          this.Iskaz = 'Good job!'
                      }
                      else if(this.BrKoraka <= 5000){
                        this.Recenica = 'Perfect';
                        this.Iskaz = 'You are a beast!'
                    }
                }
              })
        
        

            this.Koraci = this.BrKoraka;
            this.odredjenDanizabran = 0;

        /* Ukupno Kalorija Potroseno za odredjen dan*/    
        this.PotrosenoKalorija = Math.round(this.BrKoraka * 0.05);

        /* Ukupno sati i minuta  za odredjen dan*/
        this.Vreme = Math.round(this.BrKoraka * 0.5);
        const Secundi:number = this.Vreme;
        this.Sat = Math.floor(Secundi / 3600);
        this.Minuti = Math.floor(this.Vreme % 3600 / 60);

        /* Broj kilometara za odredjen dan*/
        this.Kilometari =((this.BrKoraka * 0.762)/1000)
        this.Kilometari =+ this.Kilometari.toFixed(2); 
        
        


        this.BrKoraka = 0;
      })
      
    

      

}


  


}
